'''
Created on Oct 21, 2015

@author: Daniel
'''
import settings
import sqlite3


def create_db():
    db_path = settings.DB_URL
    open(db_path, 'a').close()
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    c.execute('''CREATE TABLE
        users(username TEXT, password TEXT)
        ''')
    c.execute('''
        CREATE TABLE
        logs(username TEXT,
            title TEXT,
            description TEXT,
            uploaded DATETIME,
            path TEXT)
        ''')
    conn.commit()
    conn.close()

if __name__ == '__main__':
    create_db()
