#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on Oct 21, 2015

@author: Daniel
'''
import getpass
import os
import hashlib
import sqlite3
import settings


def create_user():
    user = raw_input("Nombre de usuario: ")
    password = getpass.getpass("Contraseña: ")
    password = hashlib.sha1(password).hexdigest()
    db_path = settings.DB_URL
    conn = sqlite3.connect(db_path)
    c = conn.cursor()
    values = (user, str(password))
    c.execute('INSERT INTO users VALUES(?, ?)',
              values)
    conn.commit()
    conn.close()

if __name__ == '__main__':
    create_user()
