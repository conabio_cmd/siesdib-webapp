'''
Created on Oct 19, 2015

@author: Daniel
'''
from flask.app import Flask
from flask_cors.extension import CORS
from flask_cors.decorator import cross_origin
from flask.globals import request
from werkzeug.utils import secure_filename
from tempfile import mkdtemp, gettempdir
import os
from flask.json import jsonify
from flask_login import login_required, LoginManager, UserMixin, login_user,\
    logout_user
import base64
from datetime import datetime
import math
from filechunkio.filechunkio import FileChunkIO
import boto3
import sqlite3
import hashlib
import json
from flask.helpers import send_from_directory
import settings


class User(UserMixin):
    '''
    classdocs
    '''
    pass


webapp = Flask(__name__)
cors = CORS(webapp)
webapp.config['CORS_HEADERS'] = 'Content-Type'

webapp.secret_key = "B614BACDB2515B3ADFBBBBF6B37F6"
login_manager = LoginManager()
login_manager.init_app(webapp)

# CHUNKSIZE de 50 MB
CHUNKSIZE = 52428800

db_path = settings.DB_URL


def upload_file_s3(bucket, fp):

    global CHUNKSIZE

    source_size = os.stat(fp).st_size

    fn = '{}-{}'.format(datetime.now().isoformat(),
                        os.path.basename(fp))
    # Name time-stamp + fp
    mpf = bucket.initiate_multipart_upload(fn)
    chunk_count = int(math.ceil(source_size / float(CHUNKSIZE)))

    for i in xrange(chunk_count):
        offset = CHUNKSIZE * i
        n_bytes = min(CHUNKSIZE, source_size - offset)

        with FileChunkIO(fp, 'rb', offset=offset, bytes=n_bytes) as f:
            mpf.upload_part_from_file(f, part_num=i + 1)

    mpf.complete_upload()
    return fn


@login_manager.user_loader
def user_loader(username):
    conn = sqlite3.connect(db_path)
    db_cursor = conn.cursor()
    values = (username)
    db_cursor.execute('''SELECT count(*) FROM users
        WHERE username=?''', values)
    user_found = db_cursor.fetchone()[0]
    conn.close()
    if user_found > 0:
        user = User()
        user.id = username
        return user
    else:
        return None


@login_manager.request_loader
def request_loader(request):
    api_key = request.headers.get('Authorization')
    if api_key:
        api_key = api_key.replace('Basic', '', 1)
        try:
            api_key = base64.b64decode(api_key)
        except TypeError:
            pass
        conn = sqlite3.connect(db_path)
        db_cursor = conn.cursor()
        values = (api_key, )
        db_cursor.execute('''SELECT count(*) FROM users
            WHERE username=?''', values)
        user_found = db_cursor.fetchone()[0]
        conn.close()
        if user_found > 0:
            user = User()
            user.id = api_key
            return user
    return None


@webapp.route('/upload_file',
              methods=['POST'])
@cross_origin()
@login_required
def upload_file():
    bucket_name = 'ecoinformatica-siesdib'
    f = request.files['file']
    data = request.values['data']
    data = json.loads(data)
    filename = secure_filename(f.filename)
    output_dir = mkdtemp()
    output_file = os.path.join(output_dir, filename)
    f.save(output_file)
    try:
        s3 = boto3.resource(service_name='s3',
                            region_name='us-west-2')
        filename = '{}-{}'.format(datetime.now().isoformat(),
                                  os.path.basename(output_file))
        (s3.Object(bucket_name, filename)
         .put(Body=open(output_file, 'rb')))
        conn = sqlite3.connect(db_path)
        db_cursor = conn.cursor()
        user = request_loader(request)
        values = (user.id,
                  data['title'],
                  data['description'],
                  os.path.join(bucket_name, filename))
        db_cursor.execute('''INSERT INTO logs
            VALUES(?, ?, ?,
                   datetime('now', 'localtime'),
                   ?)''', values)
        conn.commit()
        conn.close()
    except Exception as e:
        print(e)
        return jsonify({'valid': False})
    return jsonify({'valid': True})


@webapp.route('/get_information',
              methods=['GET'])
@cross_origin()
@login_required
def get_information():
    conn = sqlite3.connect(db_path)
    db_cursor = conn.cursor()
    information = list()
    for row in db_cursor.execute('SELECT * from logs'):
        data = {}
        data.setdefault('user', row[0])
        data.setdefault('title', row[1])
        data.setdefault('description', row[2])
        data.setdefault('date', row[3])
        data.setdefault('path', row[4])
        information.append(data)
    conn.close()
    return jsonify({'information': information})


@webapp.route('/download',
              methods=['GET'])
@cross_origin()
@login_required
def download():
    path = request.args['path']
    filename = path.split("/")[-1]
    s3 = boto3.resource(service_name='s3',
                        region_name='us-west-2')
    download_folder = mkdtemp()
    downloaded_file = os.path.join(download_folder, filename)
    s3.meta.client.download_file('ecoinformatica-siesdib',
                                 filename,
                                 downloaded_file)
    downloaded_file = downloaded_file.split("/")[-2:]
    downloaded_file = '/'.join(downloaded_file)
    return jsonify({'file': downloaded_file})


@webapp.route('/login', methods=['POST'])
@cross_origin()
def login():
    conn = sqlite3.connect(db_path)
    db_cursor = conn.cursor()
    username = request.get_json()['username']
    password = request.get_json()['password']
    password = hashlib.sha1(password).hexdigest()
    password = str(password)
    values = (username, password)
    db_cursor.execute('''SELECT count(*) FROM users
        WHERE username=? and password=?''', values)
    user_found = db_cursor.fetchone()[0]
    conn.close()
    if user_found > 0:
        user = User()
        user.id = username
        return jsonify({'valid': login_user(user, remember=True)})
    else:
        return jsonify({'valid': False})


@webapp.route('/logout', methods=['GET'])
@cross_origin()
@login_required
def logout():
    return jsonify({'valid': logout_user()})


@webapp.route('/download_file/<path:filename>')
@cross_origin()
def download_file(filename):
    return send_from_directory(gettempdir(), filename)
