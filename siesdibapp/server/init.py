#!/usr/bin/env python
'''
Created on Jul 30, 2015

@author: Daniel
'''
from tasks import webapp
from paste.translogger import TransLogger
import cherrypy


def run_server():
    app_logged = TransLogger(webapp)
    cherrypy.tree.graft(app_logged, '/')
    cherrypy.config.update({
        'engine.autoreload.on': True,
        'log.screen': True,
        'server.socket_port': 5000,
        'server.socket_host': '0.0.0.0',
        })
    cherrypy.engine.start()
    cherrypy.engine.block()


if __name__ == '__main__':
    run_server()
