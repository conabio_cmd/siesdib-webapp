FROM ubuntu
MAINTAINER Coordinación de Ecoinformática <ecoinformatica@conabio.gob.mx>

RUN apt-get update && apt-get install -y python python-pip

COPY ./siesdibapp /code
WORKDIR /code
RUN pip install -r requirements.txt
WORKDIR /code/server
RUN mkdir /data
RUN python create_db.py
RUN ls -la /data/*.db
ENTRYPOINT ["/bin/true"]
VOLUME ["/data"]
