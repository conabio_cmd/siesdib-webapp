(function(){
	
	angular.module('siesdib.config', [])
		.constant('APP_PROTOCOL', 'http')
		.constant('APP_IP', 'localhost')
		.constant('APP_PORT', '5000')
	
})();