(function () {
    'use strict';
 
    angular
    	.module('siesdib')
    	.controller('LoginController', LoginController);
 
    LoginController.$inject = ['$location', 'AuthenticationService', '$log'];
    function LoginController($location, AuthenticationService, $log) {
    	
    	render();
    	
        var vm = this;
 
        vm.login = login;
 
        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();
 
        function login() {
            vm.dataLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.valid) {
                    AuthenticationService.SetCredentials(vm.username, vm.password);
                    $location.path('/');
                } else {
                    vm.dataLoading = false;
                    swal("Datos inválidos",
                    		"Nombre de usuario y contraseña incorrectos",
                    		"error");
                }
            });
        };
    }
 
})();