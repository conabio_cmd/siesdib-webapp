(function () {
    'use strict';
 
    angular
    	.module('siesdib')
    	.controller('SearchController', SearchController);
 
    SearchController.$inject = ['$http', '$location', 'AuthenticationService', '$log',
                              'APP_PROTOCOL', 'APP_IP', 'APP_PORT'];
    
    function SearchController($http, $location, AuthenticationService, $log,
    		APP_PROTOCOL, APP_IP, APP_PORT){
    	
    	function error() {
    		swal("Hubo un error",
					"Intente nuevamente. Si el error periste, póngase en contacto con el administrador",
					"error");
    	}
    	
    	render();
    	
    	var vm = this;
    	vm.logout = logout;
    	vm.information = null;
    	vm.informationReady = false;
    	vm.downloadFile = downloadFile;
    	
    	vm.sortType = 'date';
    	vm.sortReverse = true;
    	vm.searchFilter = '';
    	
    	$http.get(APP_PROTOCOL+'://'+APP_IP+':'+APP_PORT+'/get_information')
    		.then(function(response){
    			vm.information = response.data.information;
    			vm.informationReady = true;
    		}, function(response){
    			error();
    		});
    	
    	function logout(){
    		AuthenticationService.Logout(function(response){
    			AuthenticationService.ClearCredentials();
    			$location.path('/login');
    		});
    	}
    	
    	function downloadFile(path){
    		$http.get(APP_PROTOCOL+'://'+APP_IP+':'+APP_PORT+'/download?path='+path)
    			.then(function(response){
    				location.href = APP_PROTOCOL+'://'+APP_IP+':'+APP_PORT+'/download_file/' + response.data.file;
    			}, function(response){
    				error();
    			});
    	}
    	
	}
})();