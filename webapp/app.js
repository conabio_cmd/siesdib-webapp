var render = function(){
			componentHandler.upgradeAllRegistered();
		};

(function (){
	
	var app = angular.module('siesdib', ['ngRoute', 'ngCookies', 'ngFileUpload', 'siesdib.config']);
	
	app.config(['$routeProvider', '$locationProvider',
	            function($routeProvider, $locationProvider){
		
		$routeProvider
			.when('/', {
				controller: 'HomeController',
				templateUrl: 'home/home.view.html',
				controllerAs: 'vm'
			})
			.when('/login', {
				controller: 'LoginController',
				templateUrl: 'login/login.view.html',
				controllerAs: 'vm'
			})
			.when('/search', {
				controller: 'SearchController',
				templateUrl: 'search/search.view.html',
				controllerAs: 'vm'
			})
			.otherwise({redirectTo: '/login'});
		
	}]);
	
	app.run(['$rootScope', '$location', '$cookieStore', '$http',
	         function($rootScope, $location, $cookieStore, $http){
		$rootScope.globals = $cookieStore.get('globals') || {};
		if ($rootScope.globals.currentUser){
			$http.defaults.headers.common['Authorization'] = 'Basic' + $rootScope.globals.currentUser.authdata;
		}
		
		$rootScope.$on('$locationChangeStart', function(event, next, current){
			var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
			var loggedIn = $rootScope.globals.currentUser;
			if (restrictedPage && !loggedIn){
				$location.path('/login');
			}
		});
		
	}]);
	
	
})();