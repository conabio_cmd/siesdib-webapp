(function () {
    'use strict';
 
    angular
    	.module('siesdib')
    	.controller('HomeController', HomeController);
 
    HomeController.$inject = ['$location', 'AuthenticationService', '$log', 'Upload',
                              'APP_PROTOCOL', 'APP_IP', 'APP_PORT'];
    function HomeController($location, AuthenticationService, $log, Upload,
    		APP_PROTOCOL, APP_IP, APP_PORT) {
    	
    	render();
    	
    	var vm = this;
    	vm.logout = logout;
    	vm.uploadFile = uploadFile;
    	vm.dataLoading = false;
    	vm.title = null;
    	vm.description = null;
    	vm.file = null;
    	
    	function error() {
    		swal("Hubo un error",
					"Intente nuevamente. Si el error periste, póngase en contacto con el administrador",
					"error");
    		vm.dataLoading = false;
    	}
    	
    	function logout() {
    		AuthenticationService.Logout(function(response){
    			AuthenticationService.ClearCredentials();
    			$location.path('/login');
    		});
    	}
    	
    	function uploadFile(){
    		var file = vm.file;
    		vm.dataLoading = true;
    		if (file){
    			if (!file.$error){
    				Upload.upload({
    					url: APP_PROTOCOL+'://'+APP_IP+':'+APP_PORT+'/upload_file',
    					file: file,
    					data:{
    						title: vm.title,
    						description: vm.description
    					}
    				}).then(function(response){
    					var valid = response.data.valid;
    					if (valid){
	    					vm.dataLoading = false;
	    					vm.title = null;
	    					vm.description = null;
	    					vm.file = null;
	    					render();
	    					sweetAlert("Los datos se han subido de forma correcta");
    					}
    					else
    						error();
    				}, function(response){
    					vm.dataLoading = false;
    					error();
    				});
    			}
    		}
    	}

    }
 
})();