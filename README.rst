************************************************************************************
Siesdib Frontend
************************************************************************************

Instalación y configuración
===========================

Descargue o clone el código desde el repositorio público
::

$ git clone https://bitbucket.org/conabio_cmd/siesdib-webapp.git
$ cd siesdib-webapp

Instalar las bibliotecas de javascript necesarias:
::

$ cd webapp
$ bower install --save
$ cd ../

Editar las variables de configuración:

Editar el archivo docker-compose.yml y cambiar los valores de AWS_ACCESS_KEY_ID
y AWS_SECRET_ACCESS_KEY. Estos deben ser proporcionados por el administrador del
AWS de Ecoinformática. También desde ahí se puede editar los puertos por donde correrán
las aplicaciones.

Editar el archivo webapp/config.js. Modificar los valores de APP_IP por los que fueron
asignados a la instancia de Amazon. El valor APP_PORT debe ser el mismo que el puerto
del docker cherrypy del docker-compose.yml


Compilar los dockers:
::

$ docker-compose build


Levantar los servicios:
::

$ docker-compose up -d


Creación de usuarios
===========================
Para crear un usuario, los servicios deben estar activos. Lo puede comprobar haciendo:
::

$ docker-compose ps

La salida debe decir que los servicios están corriendo. De lo contrario, vuelva a levantarlos.

Una vez comprobado lo anterior, ejecute:
::

$ docker exec -it siesdibwebapp_cherrypy_1 python /code/server/create_user.py


Consultar la base de datos
===========================
Para interactuar con la base de datos, ejecute:
::

$ docker exec -it siesdibwebapp_cherrypy_1 sqlite3 /data/siesdib.db

